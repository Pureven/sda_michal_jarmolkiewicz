/*
 * WashingMachine.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef WASHINGMACHINE_HPP_
#define WASHINGMACHINE_HPP_
#include "Machine.hpp"
#include "Timer.hpp"
#include "WashingOptions.hpp"
#include "WaterSensor.hpp"
#include "Engine.hpp"
#include "Machine.hpp"

class WashingMachine : public Machine
{
protected:
	int washTime;
	int rinseTime;
	int spineTime;
public:
	WashingMachine(int washtime, int rinsetime,int spinetime)
:washTime(washtime)
,rinseTime(rinsetime)
,spineTime(spinetime)
{

	}

	void main()
	{

	}

	void wash()
	{

	}
	void rinse()
	{
	}

	void spine()
	{
		Engine e;
		Timer t;
		e.turnOn();
		t.setDuration(3);
		t.start();
		int time= t.getValue();
		int duration=t.getDuration();
		while(time!=duration)
		{
			time=t.count();
		}
e.turnOff();

	}
	void fill()
	{

	}
	void empty()
	{

	}
	void standardWash()
	{

	}
	void twiceRinse()
	{

	}
};



#endif /* WASHINGMACHINE_HPP_ */
