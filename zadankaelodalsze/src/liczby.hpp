/*
 * liczby.hpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef LICZBY_HPP_
#define LICZBY_HPP_
#include<list>

using namespace std;

void parzysteNie(list<int> x)
{
	list<int> n, p;
	for(list<int>::iterator it=x.begin(); it!=x.end();it++)
	{
		int tmp=*it;
		if(*it%2==0)
		{
			p.push_back(tmp);
		}
		else
		{
			n.push_back(tmp);
		}
	}
	cout<<"Liczby parzyste to: ";
	for(list<int>::iterator at=p.begin(); at!=p.end();at++)
	{
		cout<<*at<<", ";
	}
	cout<<endl<<"Liczby nieparzyste to: ";
	for(list<int>::iterator nt=n.begin(); nt!=n.end(); nt++)
	{
		cout<<*nt<<", ";
	}
}


#endif /* LICZBY_HPP_ */
