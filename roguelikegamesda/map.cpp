/*
 * map.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#include <vector>
#include "map.hpp"
#include <algorithm>
namespace Game
{

bool Map::isWall(int x, int y)
{

	Funktor f1(x, y);
	std::vector<Tile>::iterator it;
	it = find_if(mTile.begin(), mTile.end(), f1);

return it->isWall;

}
void Map::setTile(int x, int y, bool isWall)
{
	Funktor f2(x, y);
	std::vector<Tile>::iterator it;
	it = std::find_if(mTile.begin(), mTile.end(), f2);
	it->isWall = isWall;


}

void Map::render(const Game::Console & console)
{
for(std::vector<Tile>::iterator it=mTile.begin(); it!=mTile.end();it++)
{
	if(it->isWall)
	{
		drawWall(console, it->mX, it->mY);
	}
	else
	{
		drawFloor(console,it->mX, it->mY);
	}
}

}

void Map::drawWall(const Game::Console& console,int x, int y)
{

	console.drawChar(176, x, y, Game::FgColour::WHITE, Game::BgColour::BLACK);
}
void Map::drawFloor(const Game::Console & console,int x, int y)
{
	BgColour::Colour bg = BgColour::BLACK;
	FgColour::Colour fg = FgColour::GREEN;

	console.drawChar(176, x, y, fg, bg);
}
}
