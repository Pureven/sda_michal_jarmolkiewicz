#include "Console.hpp"
#include "conio.h" //getch
#include <cstdio>

namespace Game
{
	Console::Console()
	{
		hConsoleOut = GetStdHandle(STD_OUTPUT_HANDLE);
	}

	Console::~Console()
	{
		clear();
		COORD coord = {0, 0};
		SetConsoleCursorPosition(hConsoleOut, coord);
	}

	void Console::setConsoleSize(int width, int height)
	{

		COORD coord;
		coord.X = width;
		coord.Y = height;

		_SMALL_RECT rect;
		rect.Top = 0;
		rect.Left = 0;
		rect.Bottom = height - 1;
		rect.Right = width - 1;

		SetConsoleScreenBufferSize(hConsoleOut, coord);
		SetConsoleWindowInfo(hConsoleOut, TRUE, &rect);

//		HWND console = GetConsoleWindow();
//		RECT r;
//
//		GetWindowRect(console, &r);
//		MoveWindow(console, r.left, r.top, width, height, TRUE);


	}

	void Console::drawChar(char ch, short int x, short int y, FgColour::Colour fg, BgColour::Colour bg) const
	{
		COORD coord = {x, y};
		SetConsoleCursorPosition(hConsoleOut, coord);
		SetConsoleTextAttribute(hConsoleOut, fg | bg);
		putchar(ch);

	}

	void Console::clear()
	{
		COORD coordScreen = { 0, 0 };    // home for the cursor
		DWORD cCharsWritten;
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		DWORD dwConSize;

		SetConsoleTextAttribute(hConsoleOut, 0x07);

		// Get the number of character cells in the current buffer.
		GetConsoleScreenBufferInfo( hConsoleOut, &csbi );

		dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

		// Fill the entire screen with blanks.
		FillConsoleOutputCharacter( hConsoleOut,     // Handle to console screen buffer
									' ',     		 // Character to write to the buffer
									dwConSize,       // Number of cells to write
									coordScreen,     // Coordinates of first cell
									&cCharsWritten );// Receive number of characters written

		GetConsoleScreenBufferInfo( hConsoleOut, &csbi );

		FillConsoleOutputAttribute( hConsoleOut,         // Handle to console screen buffer
		                                    csbi.wAttributes, // Character attributes to use
		                                    dwConSize,        // Number of cells to set attribute
		                                    coordScreen,      // Coordinates of first cell
		                                    &cCharsWritten ); // Receive number of characters written

	}

	Key Console::getKey()
	{
		char key = getch();

		if (key == 'w' || key == 'W')
		{
			return Key::MOVE_UP;
		}
		else if (key == 's' || key == 'S')
		{
			return Key::MOVE_DOWN;
		}
		else if (key == 'a' || key == 'A')
		{
			return Key::MOVE_LEFT;
		}
		else if (key == 'd' || key == 'D')
		{
			return Key::MOVE_RIGHT;
		}
		else if (key == 'q' || key == 'Q')
		{
			return Key::QUIT;
		}
		else
		{
			return Key::INVALID;
		}
	}
}
