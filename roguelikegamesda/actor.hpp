/*
 * actor.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef ACTOR_HPP_
#define ACTOR_HPP_

#include "Console.hpp"
namespace Game
{
class RougeGame;
class Actor {
private:
	int mX;
	int mY;
	char mSymbol;
	Game::FgColour::Colour mFGColor;
	Game::BgColour::Colour mBGColor;
	RougeGame* Game;
public:
	Actor(){}
	Actor(int x, int y, char symbol, Game::FgColour::Colour FGColor, Game::BgColour::Colour BGColor)
	:mX(x)
	,mY(y)
	,mSymbol(symbol)
	,mFGColor(FGColor)
	,mBGColor(BGColor)

	{

	}



	void render(Game::Console & console);
	void setPos(int x, int y);

	void move(short int x, short int y);

	void setX(int x) {
		mX = x;
	}

	void setY(int y) {
		mY = y;
	}

	int getX() const {
		return mX;
	}

	int getY() const {
		return mY;
	}

	void setSymbol(char symbol) {
		mSymbol = symbol;
	}
};
}


#endif /* ACTOR_HPP_ */
