/*
 * player.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_
#include "actor.hpp"
namespace Game
{
class Player : public Actor
{
private:
	int mHp;
public:

	Player (int hp, int x, int y, char symbol, Game::FgColour::Colour FGColor, Game::BgColour::Colour BGColor)
		:mHp(hp)
		,Actor(x, y, symbol, FGColor, BGColor)
		{
		}

Player(){}
	void changeHP( int hp)
	{
		setHp(mHp-hp);
	}

	int getHp() const {
		return mHp;
	}

	void setHp(int hp) {
		mHp = hp;
	}
};
}



#endif /* PLAYER_HPP_ */
