/*
 * actor.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#include "actor.hpp"
#include "Console.hpp"
namespace Game
{

 void Actor::render(Game::Console & console )
{
	console.drawChar(mSymbol,mX,mY,mFGColor,mBGColor);

}
void Actor::setPos(int x, int y)
{
	setX(x);
	setY(y);

}
void Actor::move(short int x, short int y)
{
	mX+=x;
	mY+=y;
}

}
