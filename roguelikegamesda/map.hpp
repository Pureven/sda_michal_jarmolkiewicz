/*
 * map.hpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */

#ifndef MAP_HPP_
#define MAP_HPP_
#include <vector>
#include "tile.hpp"
#include"Console.hpp"
namespace Game
{
class Map
{
private:
	std::vector <Tile> mTile;
	void drawWall(const Game::Console & console,int x, int y);
	void drawFloor(const Game::Console & console,int x, int y);
public:
	Map(){}
	Map(int width, int height)
{
		for (int w = 0; w < width; w++)
		{
			for (int h = 0; h < height; h++)
			{
				mTile.push_back(Tile(w,h,false));
			}
		}
}

	bool isWall(int x, int y);
	void setTile(int x, int y, bool isWall);
	void render( const Game::Console & console);

};
struct Funktor
{
	int ma, mb;
	Funktor(int a, int b)
	:ma(a), mb(b) {}
	bool operator()(Tile & t)
	{
		return t.mX==ma&& t.mY==mb;
	}
//bool operator== (Tile &t)
//		{
//	return t.mX==ma&&t.mY==mb;
//		}
};
}

#endif /* MAP_HPP_ */
