/*
 * enemy.hpp
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef ENEMY_HPP_
#define ENEMY_HPP_
#include "actor.hpp"
#include <iostream>
namespace Game
{
class Enemy : public Actor
{
private:
	int mHp;
	int dx;
	int dy;
public:
	Enemy (int hp, int x, int y, char symbol, Game::FgColour::Colour FGColor, Game::BgColour::Colour BGColor)
	:mHp(hp)
	,Actor(x, y, symbol, FGColor, BGColor)
	{

	}
	void update();

	void draw();

	int getHp() const {
		return mHp;
	}

	void setHp(int hp) {
		mHp = hp;
	}

	void setDx(int dx) {
		this->dx = dx;
	}

	void setDy(int dy) {
		this->dy = dy;
	}

	int getDx() const {
		return dx;
	}

	int getDy() const {
		return dy;
	}
};
}


#endif /* ENEMY_HPP_ */
