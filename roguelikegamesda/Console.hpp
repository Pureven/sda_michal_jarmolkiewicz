/*
 * Console.hpp
 *
 *  Created on: 01.06.2017
 *      Author: mior
 */

#ifndef CONSOLE_HPP_
#define CONSOLE_HPP_

#include "windows.h" //https://msdn.microsoft.com/en-us/library/windows/desktop/ms682073(v=vs.85).aspx

namespace Game
{
	enum class Key
	{
		MOVE_UP,
		MOVE_DOWN,
		MOVE_LEFT,
		MOVE_RIGHT,
		QUIT,
		INVALID
	};

	namespace FgColour
	{
		enum Colour
		{
			BLACK             = 0,
			DARKBLUE          = FOREGROUND_BLUE,
			DARKGREEN         = FOREGROUND_GREEN,
			DARKCYAN          = FOREGROUND_GREEN | FOREGROUND_BLUE,
			DARKRED           = FOREGROUND_RED,
			DARKMAGENTA       = FOREGROUND_RED | FOREGROUND_BLUE,
			DARKYELLOW        = FOREGROUND_RED | FOREGROUND_GREEN,
			DARKGRAY          = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
			GRAY              = FOREGROUND_INTENSITY,
			BLUE              = FOREGROUND_INTENSITY | FOREGROUND_BLUE,
			GREEN             = FOREGROUND_INTENSITY | FOREGROUND_GREEN,
			CYAN              = FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE,
			RED               = FOREGROUND_INTENSITY | FOREGROUND_RED,
			MAGENTA           = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE,
			YELLOW            = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN,
			WHITE             = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE
		};
	}

	namespace BgColour
	{
		enum Colour
		{
			BLACK             = 0,
			DARKBLUE          = BACKGROUND_BLUE,
			DARKGREEN         = BACKGROUND_GREEN,
			DARKCYAN          = BACKGROUND_GREEN | BACKGROUND_BLUE,
			DARKRED           = BACKGROUND_RED,
			DARKMAGENTA       = BACKGROUND_RED | BACKGROUND_BLUE,
			DARKYELLOW        = BACKGROUND_RED | BACKGROUND_GREEN,
			DARKGRAY          = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE,
			GRAY              = BACKGROUND_INTENSITY,
			BLUE              = BACKGROUND_INTENSITY | BACKGROUND_BLUE,
			GREEN             = BACKGROUND_INTENSITY | BACKGROUND_GREEN,
			CYAN              = BACKGROUND_INTENSITY | BACKGROUND_GREEN | BACKGROUND_BLUE,
			RED               = BACKGROUND_INTENSITY | BACKGROUND_RED,
			MAGENTA           = BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_BLUE,
			YELLOW            = BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN,
			WHITE             = BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | FOREGROUND_BLUE
		};
	}

	class Console
	{
	public:

		Console();
		~Console();

		void setConsoleSize(int width, int height);

		void drawChar(char ch, short int x, short int y, FgColour::Colour fg, BgColour::Colour bg = BgColour::BLACK) const;

		void clear();


		Game::Key getKey();

	private:
		HANDLE hConsoleOut;

	};
}
#endif /* CONSOLE_HPP_ */
