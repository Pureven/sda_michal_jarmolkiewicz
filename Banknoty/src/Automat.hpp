/*
 * Automat.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef AUTOMAT_HPP_
#define AUTOMAT_HPP_
class Automat
{
	double mKasa;
	const double banknoty [15]={500, 200,100,50,20,10,5.0,2.0,1.0,0.5,0.2,0.1,0.05, 0.02,0.01};
	int iloscBank[15];
public:
	float getKasa() const
	{
		return mKasa;
	}

	void setKasa(double kasa)
	{
		mKasa = kasa;
	}

	Automat (double kasa)
	:mKasa(kasa)
	{
		for(int k=0;k<15;k++)
		{
			iloscBank[k]=0;
		}

	}
	void ileBank();
	void wypiszbank();
};




#endif /* AUTOMAT_HPP_ */
