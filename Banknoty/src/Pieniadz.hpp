/*
 * Pieniadz.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PIENIADZ_HPP_
#define PIENIADZ_HPP_

class Pieniadz
{

	double mKasa;
	const double banknoty [15]={500, 200,100,50,20,10,5.0,2.0,1.0,0.5,0.2,0.1,0.05, 0.02,0.01};
	int iloscBank[15];
	int jakieBraki[15];

public:
	float getKasa() const
	{
		return mKasa;
	}

	void setKasa(double kasa)
	{
		mKasa = kasa;
	}

	Pieniadz (double kasa)
	:mKasa(kasa)
	{
		for(int k=0;k<15;k++)
		{
			iloscBank[k]=0;
		}

	}
	void brakibank(int n);
	void ileBank();
	void wypiszbank();
};



#endif /* PIENIADZ_HPP_ */
