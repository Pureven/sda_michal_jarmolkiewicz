/*
 * CKostka.cpp
 *
 *  Created on: Mar 23, 2017
 *      Author: orlik
 */
#include <cstdlib>
#include <ctime>

#include "CKostka.hpp"

CKostka::CKostka(unsigned int iloscScianek)
: mIloscScianek(iloscScianek)
, mWartosc()
{
	std::srand(std::time(0));
	this->losuj();
}

void CKostka::losuj()
{
	mWartosc = std::rand() % mIloscScianek + 1;
}
unsigned int  CKostka::getWartosc()
{
return mWartosc;
}
