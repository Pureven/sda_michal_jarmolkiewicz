/*
 * main.cpp
 *
 *  Created on: Mar 23, 2017
 *      Author: orlik
 */


#include <iostream>

#include "CKostka.hpp"

int main()
{
	CKostka k10(10);
	CKostka k6(6);
	CKostka k20(20);

	std::cout<<"K10="<<k10.mWartosc<<" K6="<<k6.mWartosc<<" K20="<<k20.mWartosc<<"\n";

	return 0;
}
