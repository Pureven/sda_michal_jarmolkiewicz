/*
 * Lista.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_
#include<iostream>

class Lista
{
private:
	class Wezel
	{
	public:
		int mDane;
		Wezel* nastepny;
		Wezel(int dane)
		{
			mDane = dane;
			nastepny = 0;
		}
	};
public:
	Wezel *head;
	Wezel* tail;
	Lista()
	{
		head=0;
		tail=0;
	}
	void wypisz()
	{
	     Wezel* tmp = 0;
	        tmp = head;
	        std::cout << "{ ";
	        while (tmp != 0)
	        {
	            std::cout << "[" << tmp->mDane << "] ";
	            tmp = tmp->nastepny;
	        }
	        std::cout << " }" << std::endl;
	}
	void dodajKoniec(int dane)
	{
		if(head!=0)
		{
			Wezel* tmp=new Wezel(dane);
		tail->nastepny=tmp;
		tail=tmp;
		}
		else
		{
			head=new Wezel(dane);
			tail=head;
		}
	}
	int  pobierz(int n)
	{
		Wezel * tmp=head;
		for(int i=0;i<n-1;i++)
		{
		//if(tmp==head){ std::cout<<"Nie ma takiej liczby"<<std::endl;}
		//else
		tmp=tmp->nastepny;
		}
		return tmp->mDane;
	}
	bool czyPusta()
	{
		if (tail==0) return true;
		else
		return false;
	}
	void usun(int n)
	{
		Wezel *tmp=head;
		if(n==0)
		{

			head=tmp->nastepny;
		}

		else
		{

			for(int i=0;i<n;i++)
			{
				if((i+1)==n)
				{
					tmp->nastepny=tmp->nastepny->nastepny;
				}
			}
		}
			}
	void wyczysc()
	{
		Wezel* tmp=0;
		tmp=head;
		while(tmp!=tail)
		{
			head=tmp->nastepny;
			delete tmp;
		tmp=head;

		}
		delete tail;
			tail=0;
			head=0;



	}
	void dodajPoczatek(int dane)
	{
	        if (head != 0)
	        {
	            Wezel* nowyWezel = new Wezel(dane);
	            nowyWezel->nastepny = head;
	            head = nowyWezel;
	        }
	        else
	        { //jak jest pusta
	            head = new Wezel(dane);
	            tail = head;
	        }
	    }
};




#endif /* LISTA_HPP_ */
