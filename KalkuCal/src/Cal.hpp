/*
 * Cal.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef CAL_HPP_
#define CAL_HPP_

class Calor
{
public:
	bool mPlec;
	double mWaga;
	double mWzrost;
	int mWiek;

public:
	Calor()
{
		mPlec=true;
		mWaga=0;
		mWzrost=0;
		mWiek=0;
}
	Calor(bool plec, double waga, double wzrost, int wiek)
	:mPlec(plec)
	,mWaga(waga)
	,mWzrost(wzrost)
	,mWiek(wiek)
	
	{

	}

	double calcuBMI();

	double haris();


	float getWaga() const
	{
		return mWaga;
	}

	void setWaga(float waga)
	{
		mWaga = waga;
	}

	int getWiek() const
	{
		return mWiek;
	}

	void setWiek(int wiek)
	{
		mWiek = wiek;
	}

	float getWzrost() const
	{
		return mWzrost;
	}

	void setWzrost(float wzrost)
	{
		mWzrost = wzrost;
	}

	bool isPlec() const
	{
		return mPlec;
	}

	void setPlec(bool plec)
	{
		mPlec = plec;
	}
};



#endif /* CAL_HPP_ */
