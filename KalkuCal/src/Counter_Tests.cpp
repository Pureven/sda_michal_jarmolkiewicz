/*
 * Counter_Tests.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "Counter.h"
#include"Cal.hpp"
#include"Kostka.hpp"
#include"Pass.hpp"
TEST (KOSTKA,getter)
{
	Kostka k12(12);
	EXPECT_EQ(12,k12.getScian());
}
TEST(Kostka,losuj)
{	Kostka k12(12);
	EXPECT_LT(0,k12.losuj());
	EXPECT_GE(12,k12.losuj());
}
TEST (Konstroktory,wyniki)
{
	Calor k;
	EXPECT_EQ(0,k.getWaga());
	EXPECT_EQ(0,k.getWzrost());
	EXPECT_EQ(0,k.getWiek());
	EXPECT_EQ(true, k.isPlec());
}
TEST(GETTERS,WYNIKI)
{
	Calor k;
	k.setWaga(50);
	k.setWzrost(1.60);
	k.setWiek(35);
	k.setPlec(true);
	EXPECT_EQ(50,k.getWaga());
		EXPECT_NEAR(1.60,k.getWzrost(),0.01);
		EXPECT_EQ(35,k.getWiek());
		EXPECT_EQ(true, k.isPlec());
}


TEST(BMI, wyniki) {
      Calor c(false,53,170,30);
      Calor ja(true,80.2,1.92,27);
      EXPECT_NEAR(0.00183391, c.calcuBMI(),0.02);
      EXPECT_NEAR(21.7556,ja.calcuBMI(),0.02);
}

TEST(haris, wynik)
{
	Calor ja(true,80.2,1.92,27);
	EXPECT_NEAR(992.2899, ja.haris(),0.02);
}
TEST(haslo, kurwa)
{
	Passgen c();

	EXPECT_LE(65,c.passgen());
}
int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);
      return RUN_ALL_TESTS();
}


