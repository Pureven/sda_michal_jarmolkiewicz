/*
 * Cooler.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COOLER_HPP_
#define COOLER_HPP_

class Cooler
{
public:
	virtual ~Cooler()
	{
		std::cout<<"~Cooler"<<std::endl;
	}
	virtual void blow()=0;

};



#endif /* COOLER_HPP_ */
