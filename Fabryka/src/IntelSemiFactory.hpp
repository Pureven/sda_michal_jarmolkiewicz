/*
 * IntelSemiFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef INTELSEMIFACTORY_HPP_
#define INTELSEMIFACTORY_HPP_
#include"SemiFactory.hpp"
#include"CoolerIntel.hpp"
class IntelSemiFactory : public SemiFactory
{

	public:
 Procesor* createProc()
 {
	 return new ProcesorIntel();
 }
 Cooler* createCooler()
 {
	 return new CoolerIntel();
 }
};


#endif /* INTELSEMIFACTORY_HPP_ */
