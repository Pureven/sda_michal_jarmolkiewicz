/*
 * Computer.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COMPUTER_HPP_
#define COMPUTER_HPP_
#include<iostream>
#include"Procesor.hpp"
#include"Cooler.hpp"
#include"SemiFactory.hpp"

#include<string>
class Computer
{
private:
	std::string mName;
	Procesor* mProcesor;
	Cooler* mCooler;
public:



	Computer(std::string name, SemiFactory & factory)
:mName(name)
{
 mProcesor= factory.createProc();
 mCooler= factory.createCooler();
}

	~Computer()
	{
		delete mProcesor;
		delete mCooler;
	}
	void run()
	{
		std::cout<<"Nazywam sie :"<<mName<<std::endl;
		mProcesor->process();
		mCooler->blow();
	}

};



#endif /* COMPUTER_HPP_ */
