/*
 * Procesor.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESOR_HPP_
#define PROCESOR_HPP_

class Procesor
{
public:
	virtual void process()=0;
	virtual ~Procesor()
	{
		std::cout<<"~Procesor"<<std::endl;
	}


};



#endif /* PROCESOR_HPP_ */
