/*
 * ProcesorAMD.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESORAMD_HPP_
#define PROCESORAMD_HPP_
#include"Procesor.hpp"
class ProcesorAMD : public Procesor
{

	public:
	void process()
	{
		std::cout<<"Proc AMD"<<std::endl;
	}
	~ProcesorAMD()
	{
		std::cout<<"~~Proc AMD"<<std::endl;
	}
};



#endif /* PROCESORAMD_HPP_ */
