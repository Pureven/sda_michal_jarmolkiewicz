/*
 * CoolerIntel.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COOLERINTEL_HPP_
#define COOLERINTEL_HPP_
#include"Cooler.hpp"
class CoolerIntel : public Cooler
{

	public:
	void blow()
	{
		std::cout<<"Cooler Intel "<<std::endl;
	}

	~CoolerIntel()
	{
		std::cout<<"~~Cooler Intel "<<std::endl;
	}
};




#endif /* COOLERINTEL_HPP_ */
