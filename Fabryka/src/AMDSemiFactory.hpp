/*
 * AMDSemiFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef AMDSEMIFACTORY_HPP_
#define AMDSEMIFACTORY_HPP_
#include "SemiFactory.hpp"
#include"CoolerAMD.hpp"
class AMDSemiFactory : public SemiFactory
{

	public:
	Procesor* createProc()
	{
		return new ProcesorAMD();
	}
	Cooler* createCooler()
	{
		return new CoolerAMD();
	}
};



#endif /* AMDSEMIFACTORY_HPP_ */
