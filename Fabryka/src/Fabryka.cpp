//============================================================================
// Name        : Fabryka.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<string>
#include"Procesor.hpp"
#include"ProcesorIntel.hpp"
#include"ProcesorAMD.hpp"
#include"SemiFactory.hpp"
#include"Computer.hpp"
#include"IntelSemiFactory.hpp"
#include"AMDSemiFactory.hpp"
#include "Cooler.hpp"
#include"CoolerAMD.hpp"
#include"CoolerIntel.hpp"

using namespace std;

int main() {
IntelSemiFactory intelFactory;
AMDSemiFactory amdFactory;

Computer intelPC("PC1", intelFactory);
Computer amdPC("PC2", amdFactory);

intelPC.run();
amdPC.run();

	return 0;
}
