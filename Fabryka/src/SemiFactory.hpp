/*
 * SemiFactory.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef SEMIFACTORY_HPP_
#define SEMIFACTORY_HPP_
#include "Cooler.hpp"
#include "Procesor.hpp"
class SemiFactory
{

	public:
	virtual Procesor* createProc()=0;

	virtual Cooler* createCooler()=0;

	virtual ~SemiFactory()
	{
		std::cout<<"~~SemiFactory"<<std::endl;
	}

};



#endif /* SEMIFACTORY_HPP_ */
