//============================================================================
// Name        : Klasa.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
class Data
{
protected:
	int mDzien;
	int mMiesiac;
	int mRok;
public:
	Data()
{
}
	Data(int dzien, int miesiac, int rok)
	:mDzien(dzien)
	,mMiesiac(miesiac)
	,mRok(rok)
	{
	if (dzien>31) mDzien=31;
	else mDzien=dzien;
	if(miesiac>12) mMiesiac=12;
	else mMiesiac=miesiac;
	if(rok<1700) mRok=1700;
	else mRok=rok;
	}
	void wypisz()
	{
		cout<<getDzien();
		cout << ", ";
		cout<<getMiesiac();
		cout << ", ";
		cout<<getRok();
		cout << endl;
	}
	void przesunDzien(int d)
	{
		mDzien+=d;
	}
	void przesunMiesiac(int m)
	{
		mMiesiac+=m;
	}
	void przesunRok(int r)
	{
		mRok+=r;
	}
	Data  obliczRoznice( Data a)
	{
		Data roznica;
		roznica.mDzien=mDzien-a.mDzien;
		if (roznica.mDzien < 0)
		{
			roznica.mDzien = 30 + roznica.mDzien;
			roznica.mMiesiac += 1;
		}

		roznica.mMiesiac = mMiesiac - a.mMiesiac;
		if (roznica.mMiesiac < 0)
		{
			roznica.mMiesiac = 12 + roznica.mMiesiac;
			mRok += 1;
		}

		roznica.mRok = mRok - a.mRok;
		return roznica;
	}
	int getDzien() const
	{
		return mDzien;
	}

	void setDzien(int dzien)
	{
		mDzien = dzien;
	}

	int getMiesiac() const
	{
		return mMiesiac;
	}

	void setMiesiac(int miesiac)
	{
		mMiesiac = miesiac;
	}

	int getRok() const
	{
		return mRok;
	}

	void setRok(int rok)
	{
		mRok = rok;
	}
};






//
//
//
//int main() {
//	Data pepe(1,12,1990);
//	pepe.wypisz();
//	Data hehe(3,2,1920);
//	pepe.obliczRoznice(hehe).wypisz();
//	pepe.wypisz();
//	return 0;
//}
